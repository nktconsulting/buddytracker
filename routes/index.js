var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index.html', { title: 'Express' });
});

router.get('/map', function(req, res) {
	res.render('map.html');
});



module.exports = router;
